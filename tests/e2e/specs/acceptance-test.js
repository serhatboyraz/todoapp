module.exports = {
    'should be initialization': browser => {
        browser
            .url(process.env.VUE_DEV_SERVER_URL)
            .waitForElementVisible('#app')
            .assert.elementPresent('.card-title')
            .assert.containsText('.card-title', 'Todo')
            .end()
    },
    'should be button click add todo': browser => {
        browser
            .url(process.env.VUE_DEV_SERVER_URL)
            .waitForElementVisible('#app')
            .assert.elementPresent('.todo-list-input')
            .assert.elementPresent('.todo-list-add-btn')
            .setValue('.todo-list-input','buy some milk')
            .click('.todo-list-add-btn',function (result){
                this.assert.strictEqual(result.status, 0)
            })
            .assert.containsText('.todo-list','buy some milk')
            .end()
    }


}

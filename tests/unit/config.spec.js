import {ApiHelper} from "@/api-helper";
import MockData from './mock/MockData'
describe('Configuration', () => {
    let MockAdapter = require("axios-mock-adapter");
    let mock = new MockAdapter(ApiHelper);

    mock.onGet(process.env.VUE_APP_SERVICE_URI + '/todo').reply(200, MockData.todoList)
    mock.onPut(process.env.VUE_APP_SERVICE_URI + '/todo').reply(200, MockData.todoList[2])
})

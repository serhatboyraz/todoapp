import AddToDoComponent from '@/components/AddToDoComponent'
import Vue from "vue";
import {expect} from "chai";
import sinon from "sinon";
import MockData from './mock/MockData'

describe('AddToDoComponent.vue', () => {
    let vm = null;

    beforeEach(() => {
        const container = document.createElement('div');
        const addTodoComponent = Vue.extend(AddToDoComponent);
        vm = new addTodoComponent({
            data() {
                return {
                    newToDoText: MockData.todoList[0].subject
                }
            }
        });

        vm.$mount(container)
    })

    it('should be render todo subject text box', () => {
        expect(vm.$el.querySelector('input.todo-list-input').value).to.equals(MockData.todoList[0].subject)
    })
    it('should be render todo add button', () => {
        expect(vm.$el.querySelectorAll('button.todo-list-add-btn').length).to.equals(1)
    })

    it('should be event emit when add button clicked', () => {
        const spy = sinon.spy(vm, '$emit');

        vm.$el.querySelector('.todo-list-add-btn').click();
        setTimeout(function () {
            expect(spy.called).to.be.equals(true)

            let spyCalledEventName = spy.args[0][0];
            let spyCalledData = spy.args[0][1];

            expect(spyCalledEventName).to.be.equals('todoAddedEvent');
            expect(spyCalledData.subject).to.be.equals(MockData.todoList[0].subject);
        }, 500);

    })


})

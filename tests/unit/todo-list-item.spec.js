import ToDoListItemComponent from '@/components/ToDoListItemComponent'
import Vue from "vue";
import {expect} from "chai";
import MockData from "./mock/MockData";

describe('TodoListComponent.vue', () => {
    let vm = null;
    beforeEach(() => {
        const container = document.createElement('div');
        const todoListItemComponent = Vue.extend(ToDoListItemComponent);
        vm = new todoListItemComponent({
            propsData: {
                initialData: MockData.todoList[0]
            }
        });

        vm.$mount(container)
    })

    it('should be render todo item', () => {
        expect(vm.$el.querySelectorAll('li').length).to.be.greaterThan(0)
    })
    it('should be todo item subject is buy some milk', () => {
        expect(vm.$el.querySelector('.todo-subject-label').textContent).to.be.contains(MockData.todoList[0].subject)
    })
})

import TodoListComponent from '@/components/ToDoListComponent'
import Vue from "vue";
import {expect} from "chai";

describe('TodoListComponent.vue', () => {
    let vm = null;

    beforeEach(() => {
        const container = document.createElement('div');
        const todoListComponent = Vue.extend(TodoListComponent);
        vm = new todoListComponent({});
        vm.$mount(container)
    })
    it('should be render todo list', () => {
        expect(vm.$el.querySelectorAll('.todo-list').length).to.equals(1)
    })
    it('should be render todo list items', () => {
        setTimeout(function () {
            expect(vm.$el.querySelectorAll('.todo-list-item').length).to.equals(2)
        }, 1000);

    })
})
